import math
def movement():
    lines = []
    while True:
        line = input()
        if line:
            lines.append(line)
        else:
            break
    x=0
    y=0
    for i in lines:
        res=i.split(" ")
        if(res[0]=="UP"):
            x+=int(res[1])
        elif (res[0] == "DOWN"):
            x -= int(res[1])
        elif (res[0] == "LEFT"):
            y -= int(res[1])
        elif (res[0] == "RIGHT"):
            y += int(res[1])
    return int(math.sqrt(x*x+y*y))
print(movement())