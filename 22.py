def tuplesort():
    lines = []
    while True:
        line = input()
        if line:
            lines.append(line)
        else:
            break
    lis=[]
    for i in lines:
        res=i.split(',')
        tup=tuple(filter(None, res))
        lis.append(tup)
    return (sorted(lis, key=lambda x:(str(x[0]), int(x[1]), int(x[2]))))
print(tuplesort())

'''
Tom,19,80
John,11,90
John,2,90w
Jony,17,91
Jony,17,93
Json,21,85
'''