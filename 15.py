def evendigits(x,y):
    result=""
    for i in range (x,y+1):
        count = 0
        for j in range (0,len(str(i))):
            if(int(str(i)[j])%2==0):
                count+=1;
        if(count==len(str(i))):
            result+=str(i)+","
    return result[:-1]
print(evendigits(1000,3000))