import os, sys
import imageio
import datetime

e = sys.exit

def create_gif(filenames, duration):
    images = []
    for filename in filenames:
        images.append(imageio.imread('static/'+filename))
    output_file = 'output/Gif-%s.gif' % datetime.datetime.now().strftime('%Y-%M-%d-%H-%M-%S')
    imageio.mimsave(output_file, images, duration=duration)


if __name__ == "__main__":
    duration = 0.2
    # Open a images
    imgpath = "/Users/aarthi/PycharmProjects/09_aarthi_python_problems/static/"
    filenames = sorted([x for x in os.listdir(imgpath) if x.endswith(".jpg")])
    for f in filenames:
        print(f)
    create_gif(filenames, duration)