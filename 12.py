def multilineStrings():
    lines = []
    while True:
        line = input()
        if line:
            lines.append(line)
        else:
            break
    text = '\n'.join(lines)
    print(text.upper())
    return
multilineStrings()